from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from issu_plus.core.views import HomeView, StaffCreateView
from issu_plus.tracker.views import IssueListView


urlpatterns = [
    path("admin/", admin.site.urls),
    path("new-staff/", StaffCreateView.as_view(), name="create_staff"),
    path("issues/", IssueListView.as_view(), name="issue_list"),
    path("", HomeView.as_view(), name="home"),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns

