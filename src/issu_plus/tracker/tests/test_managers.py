from datetime import datetime

from django.utils.timezone import make_aware
from django.test import TestCase

from freezegun import freeze_time

from ..models import Issue
from .factories import IssueFactory


@freeze_time("2018-9-1 12:00", tz_offset=2)
class IssueManagerTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        IssueFactory(resolved=make_aware(datetime(2018, 9, 1, 15)))
        IssueFactory(resolved=make_aware(datetime(2018, 9, 1, 17)))

    def test_average_solving_time(self):
        self.assertEqual(Issue.objects.average_solving_time().seconds, 7200)

    def test_longest_solving_time(self):
        self.assertEqual(Issue.objects.longest_solving_time().seconds, 10800)

    def test_shortest_solving_time(self):
        self.assertEqual(Issue.objects.shortest_solving_time().seconds, 3600)
