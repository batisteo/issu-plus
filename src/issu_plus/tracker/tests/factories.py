from datetime import datetime

from django.utils.timezone import now, make_aware

import factory
import factory.fuzzy

from ..choices import CATEGORY_CHOICES


class IssueFactory(factory.DjangoModelFactory):
    title = factory.Faker("bs")
    description = factory.Faker("text")
    category = factory.fuzzy.FuzzyChoice((ch[0] for ch in CATEGORY_CHOICES))
    created = factory.fuzzy.FuzzyDateTime(make_aware(datetime(2018, 9, 1)), force_year=2018, force_month=9)
    resolved = factory.fuzzy.FuzzyChoice([None, now()])

    class Meta:
        model = "tracker.Issue"
