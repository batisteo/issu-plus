from model_utils import Choices


STATUS = Choices("open", "closed")
CATEGORY_CHOICES = Choices("bug", "enhancements", "documentation")
