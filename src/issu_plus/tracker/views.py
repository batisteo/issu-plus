from django.views.generic import ListView

from .models import Issue


class IssueListView(ListView):
    model = Issue
    ordering = "-created"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["solving_time"] = {
            "average": Issue.objects.average_solving_time(),
            "longest": Issue.objects.longest_solving_time(),
            "shortest": Issue.objects.shortest_solving_time(),
        }
        return context
