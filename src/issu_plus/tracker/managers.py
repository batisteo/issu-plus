from django.db.models import Avg, Manager, F, ExpressionWrapper, DurationField


class IssueManager(Manager):
    def resolved(self):
        return (
            self.get_queryset()
            .exclude(resolved__isnull=True)
            .annotate(
                solving_time=ExpressionWrapper(
                    F("resolved") - F("created"), output_field=DurationField()
                )
            )
        )

    def average_solving_time(self):
        qs = self.resolved()
        if not qs.exists():
            return 0
        return qs.aggregate(average=Avg("solving_time"))["average"]

    def shortest_solving_time(self):
        qs = self.resolved()
        if not qs.exists():
            return 0
        return qs.order_by("solving_time")[0].solving_time

    def longest_solving_time(self):
        qs = self.resolved()
        if not qs.exists():
            return 0
        return qs.order_by("-solving_time")[0].solving_time
