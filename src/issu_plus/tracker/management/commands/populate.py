from django.core.management.base import BaseCommand

from ...tests.factories import IssueFactory


class Command(BaseCommand):
    help = "Creates some dummy issues"

    def add_arguments(self, parser):
        parser.add_argument('--quantity', default=20, type=int)

    def handle(self, *args, **options):
        for _i in range(options["quantity"]):
            IssueFactory()
