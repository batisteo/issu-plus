from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from model_utils.models import StatusModel, TimeStampedModel

from .choices import STATUS, CATEGORY_CHOICES
from .managers import IssueManager

User = get_user_model()


class Issue(TimeStampedModel, StatusModel):
    STATUS = STATUS
    CATEGORY_CHOICES = CATEGORY_CHOICES

    title = models.CharField(_("title"), max_length=100, default="")
    description = models.TextField(_("description"), blank=True)
    category = models.CharField(_("category"), max_length=20, choices=CATEGORY_CHOICES)
    resolved = models.DateTimeField(
        _("resolved"), auto_now=False, auto_now_add=False, blank=True, null=True
    )
    submitter = models.ForeignKey(
        User,
        verbose_name=_("submitter"),
        on_delete=models.SET_NULL,
        null=True,
        related_name="submitted_issues",
    )
    solver = models.ForeignKey(
        User,
        verbose_name=_("solver"),
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="resolved_issues",
    )

    objects = IssueManager()

    class Meta:
        verbose_name = _("issue")
        verbose_name_plural = _("issues")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("issue_detail", kwargs={"pk": self.pk})
