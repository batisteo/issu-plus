from django.contrib import admin
from django.utils.timezone import now, make_aware
from datetime import datetime

from .models import Issue


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    change_list_template = "change_list.html"
    list_display = ["title", "status", "category", "created", "resolved"]
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "title",
                    "description",
                    ("status", "category"),
                    "submitter",
                    "solver",
                    "resolved",
                )
            },
        ),
    )

    def changelist_view(self, request, extra_context=None):
        context = extra_context or {}
        context["solving_time"] = {
            "average": Issue.objects.average_solving_time(),
            "longest": Issue.objects.longest_solving_time(),
            "shortest": Issue.objects.shortest_solving_time(),
        }
        return super().changelist_view(request, extra_context=context)
