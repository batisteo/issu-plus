from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, Permission
from django.contrib.messages import success
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, TemplateView

from issu_plus.tracker.models import Issue

User = get_user_model()


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["latest_issues"] = Issue.objects.filter(status="open").order_by("-created")[:3]
        return context
    

class StaffCreateView(CreateView):
    model = User
    template_name = "core/user_form.html"
    form_class = UserCreationForm
    success_url = "/"

    def form_valid(self, form):
        response = super().form_valid(form)
        self.set_staff(form.cleaned_data["username"])
        success(self.request, _("Staff user successfuly created"))
        return response
    
    def set_staff(self, username):
        perm = Permission.objects.get(codename="view_issue")
        staff_group, _created = Group.objects.get_or_create(name="Staff")
        staff_group.permissions.set([perm])
        staff_group.save()
        user = User.objects.get(username=username)
        user.groups.add(staff_group)
        user.is_staff = True
        user.save()
