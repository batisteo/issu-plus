from django.urls import reverse

from django_webtest import WebTest


class HomeTests(WebTest):
    url = reverse("home")

    def test_get(self):
        response = self.app.get(self.url, status=200)
        self.assertIn("latest_issues", response.context.keys())


class UserFormTests(WebTest):
    csrf_checks = False
    url = reverse("create_staff")

    def test_get_form(self):
        response = self.app.get(self.url, status=200)
        self.assertEqual(response.form["username"].attrs["type"], "text")
        self.assertEqual(response.form["password1"].attrs["type"], "password")
        self.assertEqual(response.form["password2"].attrs["type"], "password")

    def test_post_form(self):
        form = self.app.get(self.url, status=200).form
        form["username"] = "test_user"
        form["password1"] = form["password2"] = "nějaké~heslo"
        form.submit().follow()