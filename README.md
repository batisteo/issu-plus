ISSU+
=====

_Simple issue tracker written in Django_

## Getting started

Hopefully you are on a Mac or GNU/Linux machine:

```bash
git clone git@gitlab.com:batisteo/issu-plus.git && cd issu-plus
python3 -m venv .venv && source .venv/bin/activate
pip install -r requirements/dev.txt
cp .env.example .env
./src/manage.py migrate
```

Create a superuser if you feel like it:
```bash
./src/manage.py createsuperuser
```

You can create a couple of dummy issues:
```bash
./src/manage.py populate
```

We can now run the development server:

```bash
./src/manage.py runserver
```
And then let’s open http://127.0.0.1:8000


## Tests

Running the test suite:

`./src/manage.py test issu_plus`